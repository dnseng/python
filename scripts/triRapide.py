# coding=utf-8

#  Les fonctions codées dans ce fichier permettent
# de trier une liste par l'algorithme de tri rapide
# auteur : D. Nseng
# date : 02/11/21

import random 

def nbAleatoire(T):
    '''
        Cette fonction génère un entier entre 1 et len(T)
    '''
    return random.randint(1, len(T))

def triPivotAux(T, pivot):
    '''
        - Entrée : un tableau d'entiers T et un entier pivot
        - Sortie : T modifié en plaçant tous les éléments plus 
                   petits que pivot à sa gauche (sans les tier),
                   ceux plus grands à sa droite (sans les trier)
    '''
    Tnew1 =  []
    Tnew2 =  []
    for elm in T:
        if elm < pivot:
            Tnew1 = Tnew1 + [elm]
        elif elm > pivot:
            Tnew2 = Tnew2 + [elm]
    return Tnew1 + [pivot]*nbOccurrences(T, pivot) + Tnew2

def nbOccurrences(T, elm):
    '''
        Cette fonction compte le nombre d'occurrences d'un élément
    '''
    count = 0
    for e in T :
        if e == elm:
            count += 1
    return count

def triPivot(T, pivot):
    '''
        Cette fonction est la même que triPivotAux,
        à l'exception qu'elle retourne deux tableaux
        celui à gauche et celui à droite du pivot
    '''
    TG =  []
    TD =  []
    for elm in T:
        if elm < pivot:
            TG = TG + [elm]
        elif elm > pivot:
            TD = TD + [elm]
    if len(TG) == 0:
        TG += [pivot]*nbOccurrences(T, pivot)
    else:
        TD = [pivot]*nbOccurrences(T, pivot) + TD
    return TG, TD

def triRapide(T):
    '''
        Cette fonction prend un tableau d'entiers T et le trie par tri rapide
    '''
    Tnew = []
    if len(T) <= 1:
        return T
    if len(T) == 2:
        if T[0] < T[1]:
            return T[:]
        else:
             return [T[1]] + [T[0]] 
    else:
        pivot = nbAleatoire(T)
        TG = triPivot(T, pivot)[0]
        TD = triPivot(T, pivot)[1]
    return triRapide(TG) + triRapide(TD)

## Test de fonctions
print()
print( "-------Tests de fonctions pour le tri rapide ---------")
print()
T = [1, 5, 2, 9, 1, 3, 0, -1, 2]
print()
# pivot = 2
print("triPivotAux(",T,",",2,") donne",triPivotAux(T, 2))
print()
print("triRapide(",T,") donne", triRapide(T))
# print(nbOccurrences(T, 1))
print()
print( "-------Fin Tests de fonctions pour le tri rapide ---------")
print()
# print(nbAleatoire(T))