# coding=utf-8

# Les fonctions codées dans ce fichier permettent
# de manipuler des matrices, pivot de gauss, etc.
# auteur : D. Nseng
# date : 28/10/21

def matriceToCol(A, numCol):
    '''
        - Entrée: Une matrice et un numéro de colonne
        - Sortie: la matrice colonne correspondante
    '''
    return [A[i][numCol] for i in range(len(A))]

def matriceToRow(A, numRow):
    '''
        - Entrée: Une matrice et un numéro de ligne
        - Sortie: la matrice ligne correspondante
    '''
    row = [A[numRow][j] for j in range(len(A[0]))]
    return row

def nb0Colonne(A, c):
    '''
        - Entrée : une matrice A et un entier c
        - Sortie : le nombre de cellule non nulle de la colonne c de A
    '''
    compteur = 0
    if c < len(A[0]):
        colonne = matriceToCol(A, c)
        i = 0
        while i < len(colonne):
            if colonne[i] != 0 :
                compteur += 1
            i += 1
        return compteur
    else :
        return "c doit être plus petit que le nombre de colonnes"

def nb0Ligne(A, l):
    '''
        - Entrée : une matrice A et un entier l
        - Sortie : le nombre de cellule non nulle de la ligne l de A
    '''
    compteur = 0
    if l < len(A):
        ligne = matriceToRow(A, l)
        i = 0
        while i < len(ligne):
            if ligne[i] != 0 :
                compteur += 1
            i += 1
        return compteur
    else :
        return "l doit être plus petit que le nombre de lignes !"

def gaussLigne(A, l1, l2, n1, n2):
    ligne1 = [n1*i for i in matriceToRow(A, l1)]
    ligne2 = [n2*i for i in matriceToRow(A, l2)]
    A[l1] = [ligne1[i]+ ligne2[i] for i in range(len(A))]
    return A

def sommeTrian(A):
    res =0
    i = 0
    while i < len(A):
        j = 0
        while j < len(A):
            if j > i :
                res += A[i][j]
                #print(A[i][j])
            j += 1
        i += 1
    return res
##################################################
###                       tests                ###
##################################################

A = [[1, 0, 3],
        [1, 0, 1],
        [9, 1, 4],
        [1, -1, 4]]
c = 1
l = 1
print()
print("nb0Colonne(", A, ",", c, ") donne", nb0Colonne(A, c))
print()
print("nb0Ligne(", A, ",", l, ") donne", nb0Ligne(A, l))
print()

A = [[1, 2, 3],
        [1, 5, 1],
        [9, 1, 4]]

l1 = 0; l2 = 2; n1 = 2; n2 = 10

print("gaussLigne(", A, ",", l1, ",", l2, ",", n1, ",", n2, ") donne", gaussLigne(A, l1, l2, n1, n2))
print()

A = [[1, 0, 3],
        [1, 0, 1],
        [9, 1, 4]]

print("sommeTrian(", A, ") vaut", sommeTrian(A))
print()