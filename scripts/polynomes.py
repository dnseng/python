# coding=utf-8

#  Les fonctions codées dans ce fichier permettent
# de faire quelques opérations sur les polynômes
# auteur : D. Nseng
# date : 12/11/21

def degre(T):
    '''
        Cette fonction calcul le degré d'un polynôme
    '''
    count = 1
    for elm in T:
        if elm != 0:
            count += 1
    return count

def egale(P,Q):
    '''retourne True si deux tableaux représentent le même polynôme'''
    if len(P) == len(Q):
        for i in range(len(P)):
            if P[i] != Q[i]:
                return False
        return True
    return False

def sommePoly(P, Q):
    '''
        Cette fonction calcule la somme de deux polynômes
    '''
    Tnew = []
    if len(P) >= len(Q):
        for i in range(len(Q)):
            Tnew += [P[i] + Q[i]]
        Tnew += P[len(Q):]
    else:
        for i in range(len(P)):
            Tnew += [P[i] + Q[i]]
        Tnew += Q[len(P):]
    return Tnew

def estPositif(T):
    '''
        Cette fonction test si tous les éléments de T sont positifs
    '''
    for elm in T:
        if elm < 0:
            return False  
    return True

def produitScal(P, scal):
    ''' Attendant un polynome et un scalaire retournant le produit'''
    return [scal*coef for coef in P]

def produitPoly(P, Q):
    '''
        Cette fonction calcule le produit de deux polynômes
    '''
    # On récupère le monôme de plus haut degré
    n = len(P)-1 
    m = len(Q)-1
    tailleRes = n * m 
    # le produit sera un polynôme de deg = tailleRes
    res = [0]*tailleRes
    # print(tailleRes)
    # print(res)
    if len(P) >= len(Q):
        for i in range(len(Q)):
            prod =  [0]*i + produitScal(P, Q[i])
            res = sommePoly(res, prod)
            #print(prod)
            # print("etape :",i)
            # print(res)
        return res
    else:
        for i in range(len(P)):
            prod =  [0]*i + produitScal(Q, P[i])
            res = sommePoly(res, prod)
        return res

def division(P, Q):
    '''Cette fonction fait la division euclidienne de deux polynômes'''
    
    tailleDividende = len(P)
    # tailleDiviseur = len(Q)
    Tnew = []
    quotient = []
    reste = 0
    curseur = tailleDividende-1
    while curseur > 0:
        quotient = [P[curseur]] + quotient
        Tnew = produitPoly([P[curseur]], Q)  
        Tnew = [0]*(curseur-1) + produitScal(Tnew, -1)
        P = sommePoly(P, Tnew)[:-1] # le reste R = P
        # print(P)
        curseur -= 1
    reste = P[0]
    return quotient, reste

def divisionPoly(P, Q):
    '''La division de deux polynômes'''
    return division(P, Q)[0]

def restePoly(P, Q):
    '''Le reste de la division de deux polynômes'''
    return division(P, Q)[1]

def divisionTest(P,Q):
    '''On teste si P = Q*D + R'''
    D = divisionPoly(P,Q)
    R = [restePoly(P,Q)]
    print(P, "=", Q , "*", D, "+", R, "------", egale(P, sommePoly(R, produitPoly(Q,D))))
    return "done"

## Test de fonctions
print()
print( "-------Tests de fonctions sur les polynômes ---------")
print()
T = [1, 0, 0, 1, 0, 0]
P = [1, 0, 0, 1, 0, 0]
Q = [2, 1, 1, 0, 5, 6, 5]
scal = 2
print()
print("degre(", T,") =", degre(T))
print()
print("egale(",[1,2],",", [1, 2, 0, 0],")", egale([1,2], [1,2,0,0]))
print()
print("sommePoly(", P, ",", Q, ") = ", sommePoly(P, Q))
print()
print("estPositif(", Q, ")", estPositif(Q))
print()
print("produitScal(",P,",", scal,") =", produitScal(P, scal))
print()
print("produitPoly(",[0,2,4],",",[1,0,2,3],") =", produitPoly([0,2,4],[1,0,2,3]))
print()
print("division(",[-4,3,0,2],",",[1,1],") =", division([-4,3,0,2],[1,1]))
print()
print("divisionPoly(",[-4,3,0,2],",",[1,1],") =", divisionPoly([-4,3,0,2],[1,1]))
print()
print("restePoly(",[-4,3,0,2],",",[1,1],") =", restePoly([-4,3,0,2],[1,1]))
print()
P = [-4,3,0,2]
Q = [1,1]
print(divisionTest(P,Q))
