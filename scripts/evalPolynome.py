# coding=utf-8

#  La fonction codée dans ce fichier permet
# d'évaluer un polynôme en une valeur donnée
# auteur : D. Nseng
# date : 28/10/21

def  evaluation(x, T_p):
    '''
        - Entrée : un réel x et un tableau représentant un polynôme
        - Sortie : calcule de f_p(x)
    '''
    res = 0
    i = 0
    while i < len(T_p):
        res += T_p[i]*x**i 
        i += 1
    return res

x = 2
T_p = [1, 2, 0, -4]
print()
print("evaluation(", x, ",", T_p, ") vaut", evaluation(x, T_p))
print()