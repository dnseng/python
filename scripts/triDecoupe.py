# coding=utf-8

# La fonction codée dans ce fichier permettent
# de trier un tableau d'entier par découpage
# auteur : D. Nseng
# date : 28/10/21

def decoupe(liste) :
    if len(liste) <= 1 :
        return liste
    T1 = [liste[i] for i in range(len(liste)) if liste[i] < liste[0]]
    T2 = [liste[i] for i in range(len(liste)) if liste[i] > liste[0]]
    T3 = [liste[i] for i in range(len(liste)) if liste[i] == liste[0]]

    return decoupe(T1) + T3 + decoupe(T2)


## test de la fonction

liste = [5,1,9,2,1,5,6,2]
print()
print("decoupe(", liste, ") vaut", decoupe(liste))
print()
