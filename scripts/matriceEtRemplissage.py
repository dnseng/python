# coding=utf-8

# Les fonctions codées dans ce fichier permettent
# de faire quelques opérations sur les matrices
# auteur : D. Nseng
# date : 28/10/21

def matOrdreLigne(n):
    '''
    - Entrée : un entier n
    - Sortie : une matrice carrée n x n remplie de 1 à n^2 dans l'ordre des lignes
               de droite à gauche, de haut en bas
    '''
    Tnew = []
    compteur = 1
    for i in range(n):
        ligne =  [compteur * n - j   for j in range(n)]
        Tnew = Tnew + [ligne]
        compteur += 1
    return Tnew

def matOrdreColonne(n):
    '''
    - Entrée : un entier n
    - Sortie : une matrice carrée n x n remplie de 1 à n^2 dans l'ordre des colonnes
               de haut en bas et de gauche à droite
    '''
    Tnew = []
    compteur = 1
    for i in range(n):
        ligne =  [compteur + n*j   for j in range(n)]
        Tnew = Tnew + [ligne]
        compteur += 1
    return Tnew

def matEstRemplie(matrice, n):
    '''
        - Entrée : une matrice et un entier
        - Sortie : retourne True si la matrice est remplie avec tous les entiers de 1 à n^2
                   exactement une fois chacun
    '''
    i = 0
    while i < n :
        j = 0
        while j < n :
            k = 0
            count = 0
            if matrice[i][j] >= 1 and matrice[i][j] <= n** 2:
                while k < n :
                    count += nombreOcc(matrice[k], matrice[i][j])
                    k += 1
                if count > 1 :
                    return False
            else :
                return False
            j += 1
        i += 1

    return True

def nombreOcc(Tab, elmt):
    i = 0
    cpt = 0
    for i in Tab:
        if i == elmt :
            cpt += 1
    return cpt

# Test de la fonction
n = 3

A= [[4, 7, 10],
    [2, 3, 8],
    [1, 3, 6]]

print()
print("matOrdreLigne(", n, ") donne", matOrdreLigne(n))
print()
print("matOrdreColonne(", n, ") donne", matOrdreColonne(n))
print()
print("matEstRemplie(", A, ",", n, ")", matEstRemplie(A, n))
print()
