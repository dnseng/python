# coding=utf-8

#  Les fonctions codées dans ce fichier permettent
# de faire quelques opérations sur les matrices
# auteur : D. Nseng
# date : 24/10/21

def addMatrice(A, B):
    '''
        - Entrée: deux matrices
        - Sortie: somme des deux matrices
    '''
    Tnew = []
    if len(A) == len(B):
        for i in range(len(A)):
            ligne = [A[i][j] + B[i][j] for j in range(len(A))]
            Tnew = Tnew + [ligne]
        return Tnew
    else :
        return "Les Tableaux ne sont pas de même taille, veuillez changer ! "

def prodTableau(L, C):
    '''
        - Entrée: deux tableaux
        - Sortie: somme du produit de leurs termes
    '''
    res = 0
    if len(L) == len(C) :
        for i in range(len(L)):
            res += L[i]*C[i]
        return res
    else:
        return "Les Tableaux ne sont pas de même taille, veuillez changer ! "

def matriceToCol(A, numCol):
    '''
        - Entrée: Une matrice et un numéro de colonne
        - Sortie: la matrice colonne correspondante
    '''
    matCol = [A[i][numCol] for i in range(len(A))]
    return matCol

def matriceToRow(A, numRow):
    '''
        - Entrée: Une matrice et un numéro de ligne
        - Sortie: la matrice ligne correspondante
    '''
    row = [A[numRow][j] for j in range(len(A[0]))]
    return row

def prodMatrice(A, B):
    '''
        - Entrée: deux matrices n x n
        - Sortie: le produit des deux matrices
    '''
    if len(A) == len(B) :
        Tnew = []
        for i in range(len(A)):
            ligne = [prodTableau(matriceToRow(A,i), matriceToCol(B,j))  for j in range(len(B))]
            Tnew = Tnew + [ligne]
        return Tnew
    else:
        return "Les matrices doivent être de même taille ! "

##################################################
###         Test des fonctions                 ###
##################################################

A = [[1, 2, 0],
     [3, 0, 1],
     [0, 2, 2]]

B = [[2, 0, 1],
     [0, 2, 1],
     [2, 2, 0]]

L = [2, 12, 13]

C = [2, 2, 2]

N = [2, 12, 14,  13, 0, 14, 5, 14, 12, 14, 13]

numCol = 1

numRow = 1

#Tab = [6,5,0,3,2,4,1]
print()
print("addMatrice(",A, ",", B, ") vaut \n", addMatrice(A,B))
print()
print("=======================================")
print()
print("prodTableau(", L, ",", C, ") donne", prodTableau(L,C))
print()
print("=======================================")
print()
print("matriceToCol(", A, ",", numCol,") donne", matriceToCol(A, numCol))
print()
print("matriceToRow(", A, ",", numRow,") donne", matriceToRow(A, numRow))
print()
print("=======================================")
print()
print(" ", A, " \nx ", B, "\n -----------------------------------\n = ", prodMatrice(A, B))
print()
