# coding=utf-8

# Les fonctions codées dans ce fichier permettent
# de faire quelques opérations sur les tableaux
# auteur : D. Nseng
# date : 25/10/21

def supprimerElement(Tab, entier):
    Tnew =[i for i in Tab if i!= entier]                                          
    return Tnew

def deuxiemeMax(Tab):
    elmt = max(Tab)
    Tnew = supprimerElement(Tab, elmt)
    return max(Tnew)

def nombreOccu(Tab, entier):
    resultat = len(Tab) - len(supprimerElement(Tab, entier))  
    return resultat

def nombreMax(Tab):
    res = nombreOccu(Tab, max(Tab))
    return res

##################################################
###         Test des fonctions                 ###
##################################################

N = [2, 12, 14,  13, 0, 14, 5, 14, 12, 14, 13]

print("=======================================")
print()
print("deuxiemeMax(", N, ") vaut", deuxiemeMax(N))
print()
print("=======================================")
print()
print("nombreMax(", N, ") vaut", nombreMax(N))
print()
print("=======================================")
print()