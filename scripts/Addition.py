# coding=utf-8

#  Les fonctions codées dans ce fichier permettent
# de faire quelques opérations sur les tableaux
# auteur : D. Nseng
# date : 20/10/21

import random

def genererGrdNbr(nbrChiffres):
    '''
        - Entrée : un entier nbrChiffres
        - Sortie : un entier de nbrChiffres chiffres
    '''
    return random.randint(10**(nbrChiffres-1), 10**nbrChiffres-1) 

def grdNbrToTab(entier):
    '''
        - Entrée : un entier
        - Sortie : le tableau représentant cet entier
    '''
    return [int(digit) for digit in str(entier)]

def add1(Tab1, Tab2):
    '''
        - Entrée : deux tableaux de même taille représentant des entiers
        - Sortie : le tableau représentant leur addition 
                   NB : n'oubliez pas les retenues !
    '''
    Tadd = []
    retenue = 0
    tailleTab1 = len(Tab1)
    tailleTab2 = len(Tab2)
    i = tailleTab1-1
    if tailleTab1 == tailleTab2 :
        while i > 0 :
            res = Tab1[i] + Tab2[i]
            res += retenue
            if res > 9:
                res = grdNbrToTab(res)
                retenue = res[0]
                Tadd = [res[1]] + Tadd
            else :
                Tadd = [res] + Tadd
                retenue = 0
            i -= 1
        # Addition des derniers éléments de chaque tableau (de droite à gauche)
        res = Tab1[0] + Tab2[0]
        res += retenue
        res = grdNbrToTab(res)
        return res + Tadd
    else:
        return "Vérifiez la taille des tableaux, ils doivent être de même taille !"
   
def ajustementDeTaille(Tab1, Tab2):
    '''
        - Entrée : deux tableaux représentant des entiers
        - Sortie : les tabelaux modifiés pour qu'ils aient la même taille (ajout des 0 au début du plus petit )
    '''
    Tnew = [0]
    tailleTab1 = len(Tab1)
    tailleTab2 = len(Tab2)
    if tailleTab1 > tailleTab2 :
        tabEcart = tailleTab1 - tailleTab2
        Tnew = [0]*tabEcart
        return Tnew + Tab2
    if tailleTab1 < tailleTab2 :
        tabEcart = tailleTab2 - tailleTab1
        Tnew = [0]*tabEcart
        return Tnew + Tab1
    else :
         return "Les tableaux sont de même taille !"

def grdNbrTotal(gT1, gT2):
    '''
        - Entrée : deux tableaux représentant des entiers gT1, gT2 de taille n1 et n2.
                   Ils représenteront chacun la moitié d'un entier de taille n1 + n2 - 1
        - Sortie : le tableau de l'entier total
    '''
    Tnew = []
    Tnew = Tnew + gT2[1:]
    gT2 = ajustementDeTaille(gT1, [gT2[0]])
    Tnew = add1(gT1, gT2) + Tnew
    return Tnew

def add2(Tab1, Tab2):
    Tnew = []
    return Tnew

##################################################
###         Test des fonctions                 ###
##################################################

# nbrChiffres = 5
# entier = 1945
# Tab1 = grdNbrToTab(5945)
# Tab2 = grdNbrToTab(9859)
# gT1 = [2, 3, 5, 8]
# gT2 = [2, 7, 6, 3]
# print()
# print("****** genererGrdNbr *********")
# print()
# print("genererGrdNbr(", nbrChiffres,") donne", genererGrdNbr(nbrChiffres))
# print()
# print("******* grdNbrToTab ************* ")
# print()
# print("grdNbrToTab(", entier, ") donne", grdNbrToTab(entier))
# print()
# print("********** add1 ***********")
# print()
# print("    ", Tab1 , "\n+   ", Tab2 , " \n-------------------- \n=", add1(Tab1, Tab2))
# print()
# print("********** ajustementDeTaille ***********")
# print()
# print("ajustementDeTaille(", [9, 8, 5, 9, 1, 8, 3], ",", [5, 9, 4, 5], ") vaut", ajustementDeTaille( [9, 8, 5, 9, 1, 8, 3], [5, 9, 4, 5]))
# print()
# print("********** grdNbrTotal ***********")
# print()
# print("grdNbrTotal(", gT1, ",", gT2, ") vaut", grdNbrTotal(gT1, gT2))
# print()
# print("********** add2 ***********")
# print()
