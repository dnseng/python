# coding=utf-8

# Les fonctions codées dans ce fichier permettent
# de trier une liste par l'algorithme de tri bulle
# auteur : D. Nseng
# date : 25/10/21

import random

def estTrie(T):
  '''Entree: un tableau d'entiers
    Sortie: True si le tableau est trie, False sinon  '''
  elm = T[0]
  i = 0
  while i < len(T):
    if elm > T[i]:
      return False
    else:
      i += 1 
  return True

def genererTableau(t):
  '''
      Entree: un entier
      Sortie: tableau de t entiers pris aleatoirement entre 0 et 100 
  '''
  Tres = []
  i = 0
  while i < t:
    Tres += [random.randint(0,100)]
    i = i + 1
  return Tres


def test(nbrTests,taille, tri):
  '''Entree: deux entiers nbrTests, taille et une fonction tri
    generera nbrTest tableau de taille entiers aleatoirement,
    les triera avec la fonction trie
    Sortie:  True si tout c'est bien passe, False sinon'''
  i = 1
  while i <= nbrTests:
    Tadd = genererTableau(taille)
    # print("T_",i,"=",Tadd)
    if estTrie(Tadd) == False:
        tri(Tadd)
    #   print("Tab_trie_",i,"=", tri(Tadd))
    i += 1
  return True

def triBulle(Tab):
    
    Tnew = Tab[:]
    n = len(Tnew)
    j = 1
    while j < n-1:
        i = 0
        while i < n-j:
            if Tnew[i] >= Tnew[i+1]:
                tmp = Tnew[i]
                Tnew[i] = Tnew[i+1]
                Tnew[i+1] = tmp
            i = i + 1
        j += 1
    return Tnew


##################################################
###         Test des fonctions                 ###
##################################################

N = [2, 12, 14,  13, 0, 14, 5, 14, 12, 14, 13]
nbrTests = 100
taille = len(N)
tri = triBulle
t = 5
print()
print("triBulle(", N, ") donne", triBulle(N))
print()
print("test(", nbrTests, ",", taille, ",", tri, ")", test(nbrTests,taille, tri))
print()
print("genererTableau(", t, ") donne", genererTableau(t))
print()